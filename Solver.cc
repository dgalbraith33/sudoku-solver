#include "Solver.h"

#include <ranges>

#include "absl/strings/str_format.h"
#include "common/LogicStep.h"
#include "strategies/NakedSingle.h"
#include "strategies/HiddenSingle.h"
#include "strategies/NakedDouble.h"
#include "strategies/HiddenDouble.h"
#include "strategies/NakedTriple.h"
#include "strategies/HiddenTriple.h"
#include "strategies/PointingPair.h"
#include "strategies/BoxLineReduction.h"

namespace solver {
Solver::Solver(const SudokuGrid& grid) : grid_(grid) {
  strategies_.emplace_back(new NakedSingleStrategy);
  strategies_.emplace_back(new HiddenSingleStrategy);
  strategies_.emplace_back(new NakedDoubleStrategy);
  strategies_.emplace_back(new HiddenDoubleStrategy);
  strategies_.emplace_back(new NakedTripleStrategy);
  strategies_.emplace_back(new HiddenTripleStrategy);
  strategies_.emplace_back(new PointingPairStrategy);
  strategies_.emplace_back(new BoxLineReductionStrategy);
}

absl::StatusOr<std::string> Solver::solve() {
  grid_.Initialize();
  while (!grid_.FreeIndexes().empty()) {
    auto result = Iterate();
    if (!result.ok()) {
      return result.status();
    }
    steps_.push_back(std::move(result.value()));
  }
  return grid_.DebugString();
}

absl::StatusOr<std::unique_ptr<LogicStep>> Solver::Iterate() {
  absl::Status broken = CheckForBrokenPuzzle();
  if (!broken.ok()) {
    return broken;
  }
  for (auto&& strategy: strategies_) {
    OptionalLogicStep step = strategy->Apply(grid_);
    if (step.has_value()) {
      return std::move(step.value());
    }
  }
  return absl::UnimplementedError("Exhausted all logical steps.");
}

absl::Status Solver::CheckForBrokenPuzzle() {
  auto free_list = grid_.FreeIndexes();
  auto has_zero = [this](int c) { return grid_.CellAt(c).mask().NumberRemaining() == 0; };
  auto iter = std::find_if(free_list.begin(), free_list.end(), has_zero);
  if (iter != free_list.end()) {
    return absl::InternalError(
        absl::StrCat("Broken puzzle, no candidates for r,c: ", IndexToRow(*iter) + 1, ",", IndexToColumn(*iter) + 1));
  }
  return absl::OkStatus();
}

}  // namespace solver