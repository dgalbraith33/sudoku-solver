#include <filesystem>
#include <iostream>
#include <thread>

#include "absl/container/flat_hash_map.h"
#include "absl/flags/flag.h"
#include "absl/flags/parse.h"
#include "absl/strings/str_format.h"
#include "TestCase.h"

ABSL_FLAG(std::vector<std::string>, exclude, {}, "Comma separated list of cases to exclude");

TestCase::Stats runSingleCase(const std::string& case_name, const std::string& case_path) {
  std::cout << "Starting: " << case_name << " (" << case_path << ")" << std::endl;
  TestCase test(case_name, case_path);
  test.Run();
  std::cout << "Finished: " << case_name << std::endl;
  return test.RunStats();
}

template<typename T>
class ThreadPool {
 public:
  explicit ThreadPool(int num_threads) : num_threads_(num_threads) {}
  
  void AddTask(const std::function<T()>& task) {
    if (!started_) {
      work_.push_back(task);
    }
    // Else fail silently.
  }
  
  void Start() {
    started_ = true;
    for (int i = 0; i < num_threads_; i++) {
      pool_.emplace_back(&ThreadPool::WorkThread, this);
    }
  }
  
  void Join() {
    for (std::thread& t: pool_) {
      t.join();
    }
  }
  
  const std::vector<T>& Results() {
    return results_;
  }
 
 private:
  int num_threads_;
  std::vector<std::thread> pool_;
  std::mutex results_mutex_;
  std::vector<T> results_;
  std::mutex work_mutex_;
  std::deque<std::function<T()>> work_;
  bool started_ = false;
  
  std::optional<std::function<T()>> PopNextTask() {
    std::lock_guard<std::mutex> lock(work_mutex_);
    if (work_.empty()) {
      return {};
    }
    std::function<T()> task = work_.front();
    work_.pop_front();
    return task;
  }
  
  void WorkThread() {
    auto current_task = PopNextTask();
    while (current_task.has_value()) {
      T result = current_task.value()();
      {
        std::lock_guard<std::mutex> lock(results_mutex_);
        results_.push_back(std::move(result));
      }
      current_task = PopNextTask();
    }
  }
};


int main(int argc, char** argv) {
  absl::ParseCommandLine(argc, argv);
  auto excluded = absl::GetFlag(FLAGS_exclude);
  
  auto iter = std::filesystem::directory_iterator("testcases/");
  absl::flat_hash_map<std::string, std::filesystem::path> test_cases;
  for (auto entry: iter) {
    const std::string& name = entry.path().stem().string();
    if (std::find(excluded.begin(), excluded.end(), name) == excluded.end()) {
      test_cases.emplace(name, entry.path());
    }
  }
  
  ThreadPool<TestCase::Stats> pool(4);
  for (auto& pair: test_cases) {
    pool.AddTask([&pair]() { return runSingleCase(pair.first, pair.second); });
  }
  pool.Start();
  pool.Join();
  std::vector<TestCase::Stats> stats_list = pool.Results();
  std::sort(stats_list.begin(), stats_list.end(), [](TestCase::Stats& a, TestCase::Stats& b) {
    return a.case_name < b.case_name;
  });
  std::cout << std::left << "|"
            << std::setw(30) << "Name" << "|"
            << std::right
            << std::setw(11) << "Count" << "|"
            << std::setw(11) << "Solved" << "|"
            << std::setw(11) << "Unsolved" << "|"
            << std::setw(11) << "Percent" << "|"
            << std::setw(12) << "Duration" << "|" << std::endl;
  std::cout << "|------------------------------|-----------|-----------|-----------|-----------|------------|"
            << std::endl;
  for (const TestCase::Stats& stats: stats_list) {
    std::cout << std::left << "|"
              << std::setw(30) << stats.case_name << "|"
              << std::right
              << std::setw(11) << stats.problem_count << "|"
              << std::setw(11) << stats.solved_count << "|"
              << std::setw(11) << stats.unsolved_count << "|"
              << std::setw(11) << absl::StrFormat("%.0f%%", stats.percent_solved) << "|"
              << std::setw(12) << absl::StrFormat("%0.1fμs",
                                                  absl::ToInt64Nanoseconds(stats.average_solved_duration) / 1000.0)
              << "|"
              << std::endl;
    
    if (stats.wrong_count || stats.unparsed_count || stats.broken_count || stats.unknown_count) {
      std::cout << "Error!"
                << " Unparsed: " << stats.unparsed_count
                << ", Wrong: " << stats.wrong_count
                << ", Broken: " << stats.broken_count
                << ", Unknown: " << stats.unknown_count << std::endl;
    }
  }
  return 0;
}