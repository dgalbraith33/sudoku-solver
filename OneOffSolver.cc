#include "Solver.h"
#include "grid/SudokuGrid.h"

#include <iostream>

using solver::Solver;
using solver::SudokuGrid;

int main() {
  auto grid_or = SudokuGrid::FromString("946150000000000700503080000004920000010000000000000078080040610000000057301600004");
  if (!grid_or.ok()) {
    std::cout << grid_or.status().message() << std::endl;
    return 1;
  }
  
  Solver solver(grid_or.value());
  auto solution_or = solver.solve();
  if (!solution_or.ok()) {
    std::cout << solution_or.status().message() << std::endl;
    std::cout << "Grid State:\n" << solver.GridDebugString() << std::endl;
    return 1;
  }
  
  std::string expected_solution = "946157832128396745573284196734928561815763429692415378287549613469831257351672984";
  std::cout << "Our Sol:  " << solution_or.value() << std::endl;
  std::cout << "Expected: " << expected_solution << std::endl;
  
  for (auto& step : solver.Steps()) {
    std::cout << step->DebugString() << std::endl;
  }
  
  return 0;
}