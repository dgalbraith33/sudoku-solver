import csv
import requests
import sys
import time


def get_new_puzzle(difficulty):
    url = "https://sudoku.com/api/level/" + difficulty
    headers = {
        "X-Requested-With": "XMLHttpRequest"
    }
    resp = requests.get(url, headers=headers)
    return resp.json()


def load_if_exists(file_name):
    try:
        with open(file_name, 'r', newline='') as f:
            results = {}
            reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_NONE)
            next(reader)
            for row in reader:
                results[int(row[2])] = {
                    'problem': row[0],
                    'solution': row[1]
                }
            return results
    except FileNotFoundError:
        return {}


def saturation(d):
    if len(d) < 10:
        return 0
    return len(d)/max(d.keys())


def is_saturated(d, target):
    if target == 0:
        return len(d) == max(d.keys())
    else:
        return saturation(results) >= target_saturation


def fill_results(results, difficulty, target_saturation):
    num_requests = 0
    num_successful = 0
    failure_count = 0
    while not is_saturated(results, target_saturation):
        try:
            puzzle = get_new_puzzle(difficulty)
        except Exception as e:
            failure_count += 1
            if failure_count >= 3:
                raise e
            continue
        failure_count = 0
        num_requests += 1
        if puzzle['id'] not in results.keys():
            num_successful += 1
            results[puzzle['id']] = {
                'problem': puzzle['mission'],
                'solution': puzzle['solution'],
            }
            print("Retrieved puzzle %d" % puzzle['id'])
            print("Saturation: %0.2f/%0.2f" % (saturation(results), target_saturation))
            if saturation(results) > 0.95:
                print("Remaining: %d" % (max(results.keys()) - len(results)))
            print("Attempt Ratio: %0.2f" % (num_successful/num_requests))
        time.sleep(0.02)


def write_results(results, filename):
    with open(filename, 'w', newline='') as f:
        writer = csv.writer(f, delimiter=',', quoting=csv.QUOTE_NONE)
        writer.writerow(['problem', 'solution', 'id'])
        for k, v in sorted(results.items()):
            writer.writerow([v['problem'], v['solution'], k])


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("You must supply a file path to read/write results to")
        exit(1)

    difficulty = 'easy'
    target_saturation = 0.5
    if len(sys.argv) >= 3:
        difficulty = sys.argv[2]
    if len(sys.argv) >= 4:
        target_saturation = float(sys.argv[3])

    file = sys.argv[1]
    results = load_if_exists(file)
    fill_results(results, difficulty, target_saturation)
    write_results(results, file)
