#include "Util.h"

namespace solver {

absl::flat_hash_map<int, std::vector<int>>
Util::MapValuesToPossibleSlots(SudokuGrid* grid, const std::vector<int>& search_space) {
  absl::flat_hash_map<int, std::vector<int>> value_to_possibles;
  for (int cell: search_space) {
    for (int i = 1; i <= 9; i++) {
      if (grid->CellAt(cell).mask().CanBe(i)) {
        value_to_possibles[i].push_back(cell);
      }
    }
  }
  return std::move(value_to_possibles);
}
bool Util::CellsShareRow(const std::vector<int>& search_space) {
  auto rows_dont_match = [](int a, int b) {
    return IndexToRow(a) != IndexToRow(b);
  };
  return std::adjacent_find(search_space.begin(), search_space.end(), rows_dont_match) == search_space.end();
}

bool Util::CellsShareCol(const std::vector<int>& search_space) {
  auto cols_dont_match = [](int a, int b) {
    return IndexToColumn(a) != IndexToColumn(b);
  };
  return std::adjacent_find(search_space.begin(), search_space.end(), cols_dont_match) == search_space.end();
}

bool Util::CellsShareBox(const std::vector<int>& search_space) {
  auto boxes_dont_match = [](int a, int b) {
    return IndexToBox(a) != IndexToBox(b);
  };
  return std::adjacent_find(search_space.begin(), search_space.end(), boxes_dont_match) == search_space.end();
}

} // solver