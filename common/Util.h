#ifndef SUDOKU_SOLVER_UTIL_H
#define SUDOKU_SOLVER_UTIL_H

#include <vector>

#include "absl/container/flat_hash_map.h"
#include "grid/SudokuGrid.h"

namespace solver {

class Util {
 public:
  Util() = delete;
  
  static absl::flat_hash_map<int, std::vector<int>>
  MapValuesToPossibleSlots(SudokuGrid* grid, const std::vector<int>& search_space);
  
  static bool CellsShareRow(const std::vector<int>& search_space);
  static bool CellsShareCol(const std::vector<int>& search_space);
  static bool CellsShareBox(const std::vector<int>& search_space);
};

} // solver

#endif //SUDOKU_SOLVER_UTIL_H
