#ifndef SUDOKU_SOLVER_LOGICSTEP_H
#define SUDOKU_SOLVER_LOGICSTEP_H

#include <memory>
#include <optional>
#include <string>

namespace solver {

class LogicStep {
 public:
  LogicStep(std::string grid) : grid_(std::move(grid)) {}
  virtual std::string DebugString() = 0;
 
 protected:
  std::string grid_;
};

typedef std::optional<std::unique_ptr<LogicStep>> OptionalLogicStep;

class NakedSingle : public LogicStep {
 public:
  NakedSingle(std::string grid, int index, int value) : LogicStep(std::move(grid)), index_(index), value_(value) {}
  
  std::string DebugString() override;
 
 private:
  int index_;
  int value_;
};

class HiddenSingle : public LogicStep {
 public:
  HiddenSingle(std::string grid, int index, int value) : LogicStep(std::move(grid)), index_(index), value_(value) {}
  
  std::string DebugString() override;
 
 private:
  int index_;
  int value_;
};

class NakedDouble : public LogicStep {
 public:
  NakedDouble(std::string grid, int index1, int index2, int value1, int value2) : LogicStep(std::move(grid)),
                                                                                  index1_(index1), index2_(index2),
                                                                                  value1_(value1), value2_(value2) {}
  
  std::string DebugString() override;
 
 private:
  int index1_;
  int index2_;
  int value1_;
  int value2_;
};

class HiddenDouble : public LogicStep {
 public:
  HiddenDouble(std::string grid, int index1, int index2, int value1, int value2) : LogicStep(std::move(grid)),
                                                                                   index1_(index1), index2_(index2),
                                                                                   value1_(value1), value2_(value2) {}
  
  std::string DebugString() override;
 
 private:
  int index1_;
  int index2_;
  int value1_;
  int value2_;
};

class NakedTriple : public LogicStep {
 public:
  NakedTriple(std::string grid, int index1, int index2, int index3, int value1, int value2, int value3)
    : LogicStep(std::move(grid)), index1_(index1), index2_(index2), index3_(index3), value1_(value1),
      value2_(value2), value3_(value3) {}
  
  std::string DebugString() override;
 
 private:
  int index1_;
  int index2_;
  int index3_;
  int value1_;
  int value2_;
  int value3_;
};

class HiddenTriple : public LogicStep {
 public:
  HiddenTriple(std::string grid, int index1, int index2, int index3, int value1, int value2, int value3)
      : LogicStep(std::move(grid)), index1_(index1), index2_(index2), index3_(index3), value1_(value1),
        value2_(value2), value3_(value3) {}
  
  std::string DebugString() override;
 
 private:
  int index1_;
  int index2_;
  int index3_;
  int value1_;
  int value2_;
  int value3_;
};

class PointingPair : public LogicStep {
 public:
  PointingPair() : LogicStep("") {};
  std::string DebugString() override {
    return "Pointing Pair";
  }
};

class BoxLineReduction : public LogicStep {
 public:
  BoxLineReduction() : LogicStep("") {};
  std::string DebugString() override {
    return "Box Line Reduction";
  }
};

}  // namespace solver

#endif //SUDOKU_SOLVER_LOGICSTEP_H
