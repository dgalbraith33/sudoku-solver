#include "LogicStep.h"

#include "absl/strings/str_format.h"
#include "grid/Cell.h"

namespace solver {

std::string NakedSingle::DebugString() {
  return absl::StrFormat("Found a \"naked\" %d at Row %d, Column %d. Grid:\n%s", value_, IndexToRow(index_) + 1,
                         IndexToColumn(index_) + 1, grid_);
}

std::string HiddenSingle::DebugString() {
  return absl::StrFormat("Found a \"hidden\" %d at Row %d, Column %d. Grid:\n%s", value_, IndexToRow(index_) + 1,
                         IndexToColumn(index_) + 1, grid_);
}

std::string NakedDouble::DebugString() {
  return absl::StrFormat("Found a \"naked\" double of %d and %d at Row %d, Column %d and Row %d, Column %d. Grid:\n%s",
                         value1_, value2_, IndexToRow(index1_) + 1,
                         IndexToColumn(index1_) + 1, IndexToRow(index2_) + 1,
                         IndexToColumn(index2_) + 1, grid_);
}

std::string HiddenDouble::DebugString() {
  return absl::StrFormat("Found a \"hidden\" pair of %d and %d at Row %d, Column %d and Row %d, Column %d. Grid:\n%s",
                         value1_, value2_, IndexToRow(index1_) + 1,
                         IndexToColumn(index1_) + 1, IndexToRow(index2_) + 1,
                         IndexToColumn(index2_) + 1, grid_);
}

std::string NakedTriple::DebugString() {
  return absl::StrFormat(
      "Found a \"naked\" triple of %d, %d and %d at (Row, Column)s: (%d, %d), (%d, %d), (%d, %d). Grid:\n%s",
      value1_, value2_, value3_, IndexToRow(index1_) + 1,
      IndexToColumn(index1_) + 1, IndexToRow(index2_) + 1,
      IndexToColumn(index2_) + 1, IndexToRow(index3_) + 1,
      IndexToColumn(index3_) + 1, grid_);
}

std::string HiddenTriple::DebugString() {
  return absl::StrFormat(
      "Found a \"hidden\" triple of %d, %d and %d at (Row, Column)s: (%d, %d), (%d, %d), (%d, %d). Grid:\n%s",
      value1_, value2_, value3_, IndexToRow(index1_) + 1,
      IndexToColumn(index1_) + 1, IndexToRow(index2_) + 1,
      IndexToColumn(index2_) + 1, IndexToRow(index3_) + 1,
      IndexToColumn(index3_) + 1, grid_);
}
}  // namespace solver
