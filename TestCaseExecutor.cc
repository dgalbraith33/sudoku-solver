#include <iostream>
#include <fstream>

#include "absl/flags/flag.h"
#include "absl/flags/parse.h"
#include "TestCase.h"

ABSL_FLAG(std::string, test_file, "", "CSV File to execute test cases");
ABSL_FLAG(int, max_cases_to_run, 0, "Limit on the number of cases to run. 0 (default) for unlimited.");

int main(int argc, char **argv) {
  absl::ParseCommandLine(argc, argv);
  std::string test_file = absl::GetFlag(FLAGS_test_file);
  int max_cases = absl::GetFlag(FLAGS_max_cases_to_run);
  
  if (test_file.empty()) {
    std::cout << "Must specify a test file" << std::endl;
    return 1;
  }
  
  TestCase testCase("One Off", test_file, max_cases);
  testCase.Run();
  
  TestCase::Stats stats = testCase.RunStats();
  std::cout << "Finished"
            << "\nProblems: " << stats.problem_count
            << "\nSolved: " << stats.solved_count
            << "\nUnparsed: " << stats.unparsed_count
            << "\nWrong: " << stats.wrong_count
            << "\nUnsolved: " << stats.unsolved_count
            << "\nBroken: " << stats.broken_count
            << "\nUnknown error: " << stats.unknown_count
            << "\nAverage duration to solve: " << stats.average_solved_duration
            << std::endl;
  
  if (stats.unsolved_count) {
    std::cout << "First unsolved: " << testCase.FailedCases().at(0).DebugStringPretty() << std::endl;
  }
  return 0;
}