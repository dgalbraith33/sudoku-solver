#include "Cell.h"
#include "absl/strings/str_format.h"

namespace solver {

int IndexToRow(int index) { return index / 9; }
int IndexToColumn(int index) { return index % 9; }
int IndexToBox(int index) { return ((IndexToRow(index) / 3) * 3) + (IndexToColumn(index) / 3); }

bool Cell::operator<(const Cell& other) const {
  if (mask_.NumberRemaining() == other.mask_.NumberRemaining()) {
    return index_ < other.index_;
  }
  return mask_.NumberRemaining() <= other.mask_.NumberRemaining();
}

bool Cell::ConflictsWith(const Cell& other) const {
  return (row() == other.row())
         || (col() == other.col())
         || (box() == other.box());
}

bool Cell::operator==(const Cell& other) const {
  return index_ == other.index_;
}

std::ostream& operator<<(std::ostream& out, const Cell& c) {
  out << absl::StrFormat("Cell (i,r,c,b): (%d,%d,%d,%d)", c.index_, c.row(), c.col(), c.box());
  return out;
}
}  // namespace solver