#include "SudokuGrid.h"

#include <sstream>

#include "absl/strings/str_join.h"

namespace solver {

absl::StatusOr<SudokuGrid> SudokuGrid::FromString(const std::string& grid) {
  if (grid.length() != 81) {
    return absl::InvalidArgumentError("Grid length must be 81");
  }
  std::array<int, 81> parsed_grid{};
  for (int i = 0; i < 81; i++) {
    char c = grid.at(i);
    auto index = c - '0';
    if (index < 0 || index > 9) {
      return absl::InvalidArgumentError(absl::StrCat("Invalid digit found: ", &c, " at pos: ", i));
    }
    parsed_grid[i] = static_cast<int>(index);
  }
  return SudokuGrid{parsed_grid};
}

SudokuGrid::SudokuGrid(const SudokuGrid& other) : squares_(other.squares_), grid_(other.grid_),
                                                  free_indexes_(other.free_indexes_) {}

SudokuGrid::SudokuGrid(const std::array<int, 81>& squares) : squares_(squares) {}

std::string SudokuGrid::DebugString() const {
  return absl::StrJoin(squares_, "");
}

std::string SudokuGrid::DebugStringPretty() const {
  std::ostringstream str;
  for (int i = 0; i < 81; i++) {
    if (i % 3 == 0) {
      str << '|';
    }
    if (i > 0 && i % 9 == 0) {
      str << "\n|";
    }
    if (i % 27 == 0) {
      str << "---|---|---|\n|";
    }
    str << squares_[i];
  }
  str << "|\n|---|---|---|";
  return str.str();
}

const std::vector<int>& SudokuGrid::FreeIndexes() {
  return free_indexes_;
}

void SudokuGrid::Initialize() {
  // Reserve the requisite sizes in the free lists as there was
  // a large amount of time spent extending the vectors during initialization.
  free_indexes_.reserve(81);
  for (int i = 0; i < 9; i++) {
    row_free_list_[i].reserve(9);
    col_free_list_[i].reserve(9);
    box_free_list_[i].reserve(9);
  }
  for (int i = 0; i < squares_.size(); i++) {
    grid_[i] = Cell(i, NumberMask::Any());
    if (squares_[i] == 0) {
      free_indexes_.push_back(i);
      row_free_list_[IndexToRow(i)].push_back(i);
      col_free_list_[IndexToColumn(i)].push_back(i);
      box_free_list_[IndexToBox(i)].push_back(i);
    }
  }
  for (int i = 0; i < squares_.size(); i++) {
    if (squares_[i] != 0) {
      Update(i, squares_[i]);
    }
  }
}

void SudokuGrid::Update(int index, int value) {
  squares_[index] = value;
  grid_[index] = Cell(index, value);
  Cell& cell = grid_[index];
  for (int i: row_free_list_[cell.row()]) {
    grid_[i].mutable_mask() -= value;
  }
  for (int i: col_free_list_[cell.col()]) {
    grid_[i].mutable_mask() -= value;
  }
  for (int i: box_free_list_[cell.box()]) {
    grid_[i].mutable_mask() -= value;
  }
  std::erase(free_indexes_, index);
  std::erase(row_free_list_[cell.row()], index);
  std::erase(col_free_list_[cell.col()], index);
  std::erase(box_free_list_[cell.box()], index);
}

const std::vector<int>& SudokuGrid::FreeInRow(int row) {
  return row_free_list_.at(row);
}

const std::vector<int>& SudokuGrid::FreeInCol(int col) {
  return col_free_list_.at(col);
}

const std::vector<int>& SudokuGrid::FreeInBox(int box) {
  return box_free_list_.at(box);
}

bool SudokuGrid::ApplyToCells(std::function<bool(Cell&)> apply, const std::vector<int>& cells) {
  return std::any_of(cells.begin(), cells.end(), [&apply, this](int i) { return apply(grid_[i]); });
}

OptionalLogicStep SudokuGrid::ApplyStrategy(
    std::function<OptionalLogicStep(const std::vector<int>&)> strategy) {
  for (int r = 0; r < 9; r++) {
    auto opt = strategy(FreeInRow(r));
    if (opt.has_value()) {
      return opt;
    }
  }
  for (int c = 0; c < 9; c++) {
    auto opt = strategy(FreeInCol(c));
    if (opt.has_value()) {
      return opt;
    }
  }
  for (int b = 0; b < 9; b++) {
    auto opt = strategy(FreeInBox(b));
    if (opt.has_value()) {
      return opt;
    }
  }
  return std::nullopt;
}


}  // solver