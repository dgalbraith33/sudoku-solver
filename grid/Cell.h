#ifndef SUDOKU_SOLVER_CELL_H
#define SUDOKU_SOLVER_CELL_H

#include <iostream>

#include "NumberMask.h"

namespace solver {

int IndexToRow(int index);
int IndexToColumn(int index);
int IndexToBox(int index);

class Cell {
 public:
  static Cell INVALID() { return {}; }
  Cell() : index_(-1), value_(-1), mask_(NumberMask::None()) {}
  Cell(int index, int value) : index_(index), value_(value), mask_(NumberMask::None()) {}
  Cell(int index, NumberMask mask) : index_(index), value_(0), mask_(mask) {}
  
  int index() const { return index_; }
  int row() const { return IndexToRow(index_); }
  int col() const { return IndexToColumn(index_); }
  int box() const { return IndexToBox(index_); }
  
  int value() const { return value_; }
  bool IsSolved() const { return value_ != 0; }
  
  NumberMask& mutable_mask() { return mask_; }
  const NumberMask& mask() const { return mask_; }
  
  bool ConflictsWith(const Cell& other) const;
  
  bool operator<(const Cell& other) const;
  bool operator==(const Cell& other) const;
  
  friend std::ostream& operator<<(std::ostream& out, const Cell& c);
 
 private:
  int index_;
  int value_;
  NumberMask mask_;
};


}  // namespace solver

#endif //SUDOKU_SOLVER_CELL_H
