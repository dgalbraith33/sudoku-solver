#include "NumberMask.h"

namespace solver {
namespace {

short CalcRemaining(short number) {
  short possibles = 0;
  for (int i = 0; i < 9; i++) {
    if (((0x1 << i) & number) != 0) {
      possibles++;
    }
  }
  return possibles;
}

}  // namespace

bool NumberMask::CanBe(short possibility) const {
  return (0x1 << (possibility - 1)) & mask_;
}

bool NumberMask::Intersects(const NumberMask& other) const {
  return mask_ & other.mask_;
}

void NumberMask::RemoveOther(const NumberMask& other) {
  mask_ &= ~other.mask_;
}

short NumberMask::FirstPossible() const {
  for (int i = 0; i < 9; i++) {
    if (((0x1 << i) & mask_) != 0) {
      return i + 1;
    }
  }
  return 0;
}

NumberMask::NumberMask(short mask) : mask_(mask) {
  remaining_ = CalcRemaining(mask_);
}

bool NumberMask::operator==(const NumberMask& other) const {
  return mask_ == other.mask_;
}

NumberMask NumberMask::operator|(const NumberMask& other) const {
  return NumberMask(mask_ | other.mask_);
}
void NumberMask::operator|=(const NumberMask& other) {
  mask_ |= other.mask_;
  remaining_ = CalcRemaining(mask_);
}

NumberMask NumberMask::operator&(const NumberMask& other) const {
  return NumberMask(mask_ & other.mask_);
}
void NumberMask::operator&=(const NumberMask& other) {
  mask_ &= other.mask_;
  remaining_ = CalcRemaining(mask_);
}

NumberMask NumberMask::operator-(int value) const {
  NumberMask mask = *this;
  mask -= value;
  return mask;
}
void NumberMask::operator-=(int value) {
  mask_ &= ~(0x1 << (value - 1));
  remaining_ = CalcRemaining(mask_);
}
NumberMask NumberMask::operator+(int value) const {
  NumberMask mask = *this;
  mask += value;
  return mask;
}
void NumberMask::operator+=(int value) {
  mask_ |= (0x1 << (value - 1));
  remaining_ = CalcRemaining(mask_);
}

} // solver