#ifndef SUDOKU_SOLVER_NUMBERMASK_H
#define SUDOKU_SOLVER_NUMBERMASK_H

namespace solver {

// Stores the possible numbers for a square in a mask that can be efficiently stored and modified.
class NumberMask {
 public:
  static NumberMask Any() {
    return NumberMask(0x1FF, 9);
  }
  static NumberMask None() {
    return NumberMask(0x0, 0);
  }
  
  // Prefer to use the Any factory method for clarity.
  // This method really just exists to make it possible to construct a key value by default in a map.
  NumberMask() : mask_(0x1FF), remaining_(9) {}
  
  bool CanBe(short possibility) const;
  bool Intersects(const NumberMask& other) const;
  
  void RemoveOther(const NumberMask& other);
  
  short NumberRemaining() const { return remaining_; }
  short FirstPossible() const;
  
  bool operator==(const NumberMask& other) const;
  
  NumberMask operator|(const NumberMask& other) const;
  void operator|=(const NumberMask& other);
  
  NumberMask operator&(const NumberMask& other) const;
  void operator&=(const NumberMask& other);
  
  NumberMask operator+(int value) const;
  void operator+=(int value);
  
  NumberMask operator-(int value) const;
  void operator-=(int value);
 
 private:
  explicit NumberMask(short mask, short remaining) : mask_(mask), remaining_(remaining) {}
  explicit NumberMask(short mask);
  
  short mask_;
  short remaining_;
};

} // solver

#endif //SUDOKU_SOLVER_NUMBERMASK_H
