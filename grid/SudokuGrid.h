#ifndef SUDOKU_SOLVER_SUDOKUGRID_H
#define SUDOKU_SOLVER_SUDOKUGRID_H

#include <array>
#include <ranges>
#include <string>

#include "absl/status/statusor.h"
#include "Cell.h"
#include "NumberMask.h"
#include "common/LogicStep.h"

namespace solver {

class SudokuGrid {
 public:
  static absl::StatusOr<SudokuGrid> FromString(const std::string& grid);
  SudokuGrid(const SudokuGrid& other);
  void Initialize();
  
  const std::vector<int>& FreeIndexes();
  const std::vector<int>& FreeInRow(int row);
  const std::vector<int>& FreeInCol(int col);
  const std::vector<int>& FreeInBox(int box);
  void Update(int index, int value);
  
  std::string DebugString() const;
  std::string DebugStringPretty() const;
  
  const Cell& CellAt(int index) { return grid_[index]; }
  bool ApplyToCells(std::function<bool(Cell&)> apply, const std::vector<int>& cells);
  
  OptionalLogicStep ApplyStrategy(std::function<OptionalLogicStep(const std::vector<int>&)> constraint);
 
 private:
  explicit SudokuGrid(const std::array<int, 81>& squares);
  
  std::array<int, 81> squares_;
  std::array<Cell, 81> grid_;
  std::vector<int> free_indexes_;
  
  std::array<std::vector<int>, 9> row_free_list_;
  std::array<std::vector<int>, 9> col_free_list_;
  std::array<std::vector<int>, 9> box_free_list_;
};

} // solver

#endif //SUDOKU_SOLVER_SUDOKUGRID_H
