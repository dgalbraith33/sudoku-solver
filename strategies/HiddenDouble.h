#ifndef SUDOKU_SOLVER_HIDDENDOUBLE_H
#define SUDOKU_SOLVER_HIDDENDOUBLE_H

#include "Strategy.h"

namespace solver {

class HiddenDoubleStrategy : public Strategy {
 public:
  OptionalLogicStep Apply(SudokuGrid& grid) override;
  static OptionalLogicStep CheckForHiddenDoubleInSet(SudokuGrid* grid, const std::vector<int>& search_space);
};

} // solver

#endif //SUDOKU_SOLVER_HIDDENDOUBLE_H
