#include "NakedDouble.h"

namespace solver {
namespace {

std::vector<std::pair<int, int>> FindPairs(SudokuGrid* grid, const std::vector<int>& search_space) {
  std::vector<std::pair<int, int>> results;
  for (int i = 0; i < search_space.size(); i++) {
    const Cell& cell_i = grid->CellAt(search_space.at(i));
    if (cell_i.mask().NumberRemaining() == 2) {
      for (int j = i + 1; j < search_space.size(); j++) {
        const Cell& cell_j = grid->CellAt(search_space.at(j));
        if (cell_j.mask().NumberRemaining() == 2 &&
            cell_j.mask() == cell_i.mask()) {
          results.emplace_back(i, j);
        }
      }
    }
  }
  return results;
}

bool RemoveIntersectionIfNotMatches(const Cell& a, const Cell& b, Cell& c) {
  if (c != a && c != b && c.mask().Intersects(a.mask())) {
    c.mutable_mask().RemoveOther(a.mask());
    return true;
  }
  return false;
}

}  // namespace

OptionalLogicStep
NakedDoubleStrategy::CheckForNakedDoubleInSet(SudokuGrid* grid, const std::vector<int>& search_space) {
  if (search_space.size() < 4) {
    // If there are only 2 free cells then finding the double does nothing.
    // If there are 3 free cells and a naked double then the 3rd cell would have a hidden single which would be
    // picked up by the earlier algorithm.
    return std::nullopt;
  }
  for (auto [i, j]: FindPairs(grid, search_space)) {
    const Cell& cell_i = grid->CellAt(search_space.at(i));
    const Cell& cell_j = grid->CellAt(search_space.at(j));
    auto remove_poss = std::bind_front(RemoveIntersectionIfNotMatches, cell_i, cell_j);
    auto naked_double = std::make_unique<NakedDouble>("", cell_i.index(), cell_j.index(), 0, 0);
    if (grid->ApplyToCells(remove_poss, search_space)) {
      return naked_double;
    }
  }
  return std::nullopt;
}

OptionalLogicStep NakedDoubleStrategy::Apply(SudokuGrid& grid) {
  return grid.ApplyStrategy(std::bind_front(&NakedDoubleStrategy::CheckForNakedDoubleInSet, &grid));
}

} // solver