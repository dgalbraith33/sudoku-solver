//
// Created by drew on 5/16/22.
//

#include "HiddenTriple.h"
#include "absl/container/flat_hash_map.h"
#include "common/Util.h"
#include "absl/strings/str_join.h"

namespace solver {

OptionalLogicStep
HiddenTripleStrategy::CheckForHiddenTripleInSet(SudokuGrid* grid, const std::vector<int>& search_space) {
  auto value_to_possibles = Util::MapValuesToPossibleSlots(grid, search_space);
  auto iter = value_to_possibles.begin();
  for (auto iter_i = value_to_possibles.begin(); iter_i != value_to_possibles.end(); iter_i++) {
    int i_val = iter_i->first;
    auto& i_poss = iter_i->second;
    if (i_poss.size() != 3) {
      continue;
    }
    for (auto iter_j = std::next(iter_i); iter_j != value_to_possibles.end(); iter_j++) {
      int j_val = iter_j->first;
      auto& j_poss = iter_j->second;
      if (j_poss != i_poss) {
        continue;
      }
      for (auto iter_k = std::next(iter_j); iter_k != value_to_possibles.end(); iter_k++) {
        int k_val = iter_k->first;
        auto& k_poss = iter_k->second;
        if (k_poss != i_poss) {
          continue;
        }
        // We have found a hidden triple!
        NumberMask mask = NumberMask::None() + i_val + j_val + k_val;
        auto set_mask = [&mask](Cell& c) {
          int initial = c.mask().NumberRemaining();
          c.mutable_mask() &= mask;
          return initial != c.mask().NumberRemaining();
        };
        int a = i_poss[0];
        int b = i_poss[1];
        int c = i_poss[2];
        auto hidden_triple = std::make_unique<HiddenTriple>("", a, b, c, i_val, j_val, k_val);
        if (grid->ApplyToCells(set_mask, i_poss)) {
          return hidden_triple;
        }
      }
    }
  }
  return std::nullopt;
}

OptionalLogicStep HiddenTripleStrategy::Apply(SudokuGrid& grid) {
  return grid.ApplyStrategy(std::bind_front(&HiddenTripleStrategy::CheckForHiddenTripleInSet, &grid));
}

} // solver