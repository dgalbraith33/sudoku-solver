//
// Created by drew on 5/16/22.
//

#include "PointingPair.h"
#include "absl/container/flat_hash_map.h"
#include "common/Util.h"

namespace solver {
namespace {

bool RemoveIntersectionIfNotInBox(int box, int value, Cell& c) {
  if (c.box() != box) {
    int cnt = c.mask().NumberRemaining();
    c.mutable_mask() -= value;
    return cnt != c.mask().NumberRemaining();
  }
  return false;
}

}  // namespace

OptionalLogicStep PointingPairStrategy::Apply(SudokuGrid& grid) {
  for (int box = 0; box < 9; box++) {
    auto value_to_possibles = Util::MapValuesToPossibleSlots(&grid,
                                                             grid.FreeInBox(box));
    for (auto iter = value_to_possibles.begin(); iter != value_to_possibles.end(); iter++) {
      int value = iter->first;
      auto& possible_slots = iter->second;
      if (Util::CellsShareRow(possible_slots)) {
        auto remove_poss = std::bind_front(RemoveIntersectionIfNotInBox, box, value);
        if (grid.ApplyToCells(remove_poss, grid.FreeInRow(IndexToRow(possible_slots.at(0))))) {
          return std::make_unique<PointingPair>();
        }
      }
      if (Util::CellsShareCol(possible_slots)) {
        auto remove_poss = std::bind_front(RemoveIntersectionIfNotInBox, box, value);
        if (grid.ApplyToCells(remove_poss, grid.FreeInCol(IndexToColumn(possible_slots.at(0))))) {
          return std::make_unique<PointingPair>();
        }
      }
    }
  }
  return absl::nullopt;
}
} // solver