#ifndef SUDOKU_SOLVER_BOXLINEREDUCTION_H
#define SUDOKU_SOLVER_BOXLINEREDUCTION_H

#include "Strategy.h"

namespace solver {

class BoxLineReductionStrategy : public Strategy {
 public:
  OptionalLogicStep Apply(SudokuGrid &grid) override;

};

} // solver

#endif //SUDOKU_SOLVER_BOXLINEREDUCTION_H
