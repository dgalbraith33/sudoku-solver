#include "BoxLineReduction.h"
#include "common/Util.h"

namespace solver {
namespace {

bool RemoveIntersectionIfNotInRow(int row, int value, Cell& c) {
  if (c.row() != row) {
    int cnt = c.mask().NumberRemaining();
    c.mutable_mask() -= value;
    return cnt != c.mask().NumberRemaining();
  }
  return false;
}

bool RemoveIntersectionIfNotInCol(int col, int value, Cell& c) {
  if (c.col() != col) {
    int cnt = c.mask().NumberRemaining();
    c.mutable_mask() -= value;
    return cnt != c.mask().NumberRemaining();
  }
  return false;
}

}  // namespace

OptionalLogicStep BoxLineReductionStrategy::Apply(SudokuGrid& grid) {
  // Check Rows
  for (int row = 0; row < 9; row++) {
    auto value_to_possibles = Util::MapValuesToPossibleSlots(&grid, grid.FreeInRow(row));
    for (auto& pair: value_to_possibles) {
      int value = pair.first;
      auto& possibles = pair.second;
      if (possibles.empty()) {
        continue;
      }
      if (Util::CellsShareBox(possibles)) {
        auto remove_poss = std::bind_front(RemoveIntersectionIfNotInRow, row, value);
        if (grid.ApplyToCells(remove_poss, grid.FreeInBox(IndexToBox(possibles.at(0))))) {
          return std::make_unique<PointingPair>();
        }
      }
    }
  }
  
  // Check Columns
  for (int col = 0; col < 9; col++) {
    auto value_to_possibles = Util::MapValuesToPossibleSlots(&grid, grid.FreeInCol(col));
    for (auto& pair: value_to_possibles) {
      int value = pair.first;
      auto& possibles = pair.second;
      if (Util::CellsShareBox(possibles)) {
        auto remove_poss = std::bind_front(RemoveIntersectionIfNotInCol, col, value);
        if (grid.ApplyToCells(remove_poss, grid.FreeInBox(IndexToBox(possibles.at(0))))) {
          return std::make_unique<PointingPair>();
        }
      }
    }
  }
  return std::nullopt;
}

} // solver