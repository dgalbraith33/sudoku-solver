#include "HiddenDouble.h"

#include "absl/container/flat_hash_map.h"
#include "Solver.h"
#include "common/Util.h"

namespace solver {

OptionalLogicStep
HiddenDoubleStrategy::CheckForHiddenDoubleInSet(SudokuGrid* grid, const std::vector<int>& search_space) {
  auto value_to_possibles = Util::MapValuesToPossibleSlots(grid, search_space);
  
  for (auto iter_i = value_to_possibles.begin(); iter_i != value_to_possibles.end(); iter_i++) {
    int i_val = iter_i->first;
    auto& i_poss = iter_i->second;
    if (i_poss.size() != 2) {
      continue;
    }
    for (auto iter_j = std::next(iter_i); iter_j != value_to_possibles.end(); iter_j++) {
      int j_val = iter_j->first;
      auto& j_poss = iter_j->second;
      if (i_poss != j_poss) {
        continue;
      }
      // We have found a hidden double!
      NumberMask mask = NumberMask::None() + i_val + j_val;
      auto set_mask = [&mask](Cell& c) {
        int initial = c.mask().NumberRemaining();
        c.mutable_mask() &= mask;
        return initial != c.mask().NumberRemaining();
      };
      int a = i_poss[0];
      int b = i_poss[1];
      auto hidden_double = std::make_unique<HiddenDouble>("", a, b, i_val, j_val);
      if (grid->ApplyToCells(set_mask, i_poss)) {
        return hidden_double;
      }
    }
  }
  return std::nullopt;
}

OptionalLogicStep HiddenDoubleStrategy::Apply(SudokuGrid& grid) {
  return grid.ApplyStrategy(std::bind_front(&HiddenDoubleStrategy::CheckForHiddenDoubleInSet, &grid));
}


} // solver