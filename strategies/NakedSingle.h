#ifndef SUDOKU_SOLVER_NAKEDSINGLE_H
#define SUDOKU_SOLVER_NAKEDSINGLE_H

#include "Strategy.h"

namespace solver {

class NakedSingleStrategy : public Strategy {
 public:
  OptionalLogicStep Apply(SudokuGrid &grid) override;
};

} // solver

#endif //SUDOKU_SOLVER_NAKEDSINGLE_H
