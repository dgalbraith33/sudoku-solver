#ifndef SUDOKU_SOLVER_NAKEDDOUBLE_H
#define SUDOKU_SOLVER_NAKEDDOUBLE_H

#include "common/LogicStep.h"
#include "Strategy.h"

namespace solver {

class NakedDoubleStrategy : public Strategy {
 public:
  OptionalLogicStep Apply(SudokuGrid& grid) override;
  static OptionalLogicStep CheckForNakedDoubleInSet(SudokuGrid* grid, const std::vector<int>& search_space);
};

} // solver

#endif //SUDOKU_SOLVER_NAKEDDOUBLE_H
