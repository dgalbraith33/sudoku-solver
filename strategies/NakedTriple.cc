#include "NakedTriple.h"

namespace solver {
namespace {

bool RemoveTripleIntersectionIfNotMatches(const Cell& a, const Cell& b, const Cell& c, Cell& d) {
  NumberMask triple = a.mask() | b.mask() | c.mask();
  if (d != a && d != b && d != c && d.mask().Intersects(triple)) {
    d.mutable_mask().RemoveOther(triple);
    return true;
  }
  return false;
}


std::vector<std::tuple<int, int, int>> FindTriples(SudokuGrid* grid, const std::vector<int>& search_space) {
  std::vector<std::tuple<int, int, int>> results;
  for (int i = 0; i < search_space.size(); i++) {
    const Cell& cell_i = grid->CellAt(search_space.at(i));
    if (cell_i.mask().NumberRemaining() <= 3) {
      for (int j = i + 1; j < search_space.size(); j++) {
        const Cell& cell_j = grid->CellAt(search_space.at(j));
        if ((cell_i.mask() | cell_j.mask()).NumberRemaining() <= 3) {
          for (int k = j + 1; k < search_space.size(); k++) {
            const Cell& cell_k = grid->CellAt(search_space.at(k));
            if ((cell_i.mask() | cell_j.mask() | cell_k.mask()).NumberRemaining() <= 3) {
              results.emplace_back(i, j, k);
            }
          }
        }
      }
    }
  }
  return results;
}


}  // namespace

OptionalLogicStep
NakedTripleStrategy::CheckForNakedTripleInSet(SudokuGrid* grid, const std::vector<int>& search_space) {
  if (search_space.size() < 5) {
    // If there are only 3 free cells then finding the double does nothing.
    // If there are 4 free cells and a naked triple then the 4th cell would have a hidden single which would be
    // picked up by the earlier algorithm.
    return std::nullopt;
  }
  for (auto [i, j, k]: FindTriples(grid, search_space)) {
    const Cell& cell_i = grid->CellAt(search_space.at(i));
    const Cell& cell_j = grid->CellAt(search_space.at(j));
    const Cell& cell_k = grid->CellAt(search_space.at(k));
    auto remove_poss = std::bind_front(RemoveTripleIntersectionIfNotMatches, cell_i, cell_j, cell_k);
    auto naked_triple = std::make_unique<NakedTriple>("", cell_i.index(), cell_j.index(),
                                                      cell_k.index(), 0, 0, 0);
    if (grid->ApplyToCells(remove_poss, search_space)) {
      return naked_triple;
    }
  }
  return std::nullopt;
}

OptionalLogicStep NakedTripleStrategy::Apply(SudokuGrid& grid) {
  return grid.ApplyStrategy(std::bind_front(&NakedTripleStrategy::CheckForNakedTripleInSet, &grid));
}

} // solver