#ifndef SUDOKU_SOLVER_STRATEGY_H
#define SUDOKU_SOLVER_STRATEGY_H

#include "grid/SudokuGrid.h"

namespace solver {

class Strategy {
 public:
  virtual OptionalLogicStep Apply(SudokuGrid& grid) = 0;
};

} // solver

#endif //SUDOKU_SOLVER_STRATEGY_H
