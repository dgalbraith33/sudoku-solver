//
// Created by drew on 5/16/22.
//

#ifndef SUDOKU_SOLVER_POINTINGPAIR_H
#define SUDOKU_SOLVER_POINTINGPAIR_H

#include "Strategy.h"

namespace solver {

class PointingPairStrategy : public Strategy {
 public:
  OptionalLogicStep Apply(SudokuGrid &grid) override;
};

} // solver

#endif //SUDOKU_SOLVER_POINTINGPAIR_H
