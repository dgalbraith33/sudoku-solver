//
// Created by drew on 5/16/22.
//

#ifndef SUDOKU_SOLVER_HIDDENTRIPLE_H
#define SUDOKU_SOLVER_HIDDENTRIPLE_H

#include "Strategy.h"

namespace solver {

class HiddenTripleStrategy : public Strategy {
 public:
  OptionalLogicStep Apply(SudokuGrid& grid) override;
  static OptionalLogicStep CheckForHiddenTripleInSet(SudokuGrid* grid, const std::vector<int>& search_space);
};

} // solver

#endif //SUDOKU_SOLVER_HIDDENTRIPLE_H
