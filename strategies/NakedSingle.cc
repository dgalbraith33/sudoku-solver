#include "NakedSingle.h"

namespace solver {

OptionalLogicStep NakedSingleStrategy::Apply(SudokuGrid& grid) {
  const auto& free_list = grid.FreeIndexes();
  auto has_one = [&grid](int c) { return grid.CellAt(c).mask().NumberRemaining() == 1; };
  auto iter = std::find_if(free_list.begin(), free_list.end(), has_one);
  if (iter != free_list.end()) {
    int value = grid.CellAt(*iter).mask().FirstPossible();
    auto naked_single = std::make_unique<NakedSingle>("", *iter, value);
    grid.Update(*iter, value);
    return naked_single;
  }
  return std::nullopt;
}

} // solver