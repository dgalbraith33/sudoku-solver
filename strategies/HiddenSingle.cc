#include "HiddenSingle.h"

namespace solver {

namespace {
NumberMask PossibleInSet(SudokuGrid* grid, const std::vector<int>& search_space) {
  NumberMask mask = NumberMask::None();
  for (int i: search_space) {
    mask |= grid->CellAt(i).mask();
  }
  return mask;
}

}  // namespace

OptionalLogicStep
HiddenSingleStrategy::CheckForHiddenSingleInSet(SudokuGrid* grid, const std::vector<int>& search_space) {
  NumberMask m = PossibleInSet(grid, search_space);
  for (short value = 1; value <= 9; value++) {
    if (!m.CanBe(value)) {
      continue;
    }
    auto can_be = [value, grid](int i) { return grid->CellAt(i).mask().CanBe(value); };
    auto iter = std::find_if(search_space.begin(), search_space.end(), can_be);
    if (iter != search_space.end()) {
      if (std::find_if(iter + 1, search_space.end(), can_be) == search_space.end()) {
        auto hidden_single = std::make_unique<HiddenSingle>("", *iter, value);
        grid->Update(*iter, value);
        return hidden_single;
      }
    }
  }
  return std::nullopt;
}


OptionalLogicStep HiddenSingleStrategy::Apply(SudokuGrid& grid) {
  return grid.ApplyStrategy(std::bind_front(&HiddenSingleStrategy::CheckForHiddenSingleInSet, &grid));
}
} // solver