//
// Created by drew on 5/16/22.
//

#ifndef SUDOKU_SOLVER_NAKEDTRIPLE_H
#define SUDOKU_SOLVER_NAKEDTRIPLE_H

#include "Strategy.h"

namespace solver {

class NakedTripleStrategy : public Strategy {
 public:
  OptionalLogicStep Apply(SudokuGrid& grid) override;
  static OptionalLogicStep CheckForNakedTripleInSet(SudokuGrid* grid, const std::vector<int>& search_space);
};

} // solver

#endif //SUDOKU_SOLVER_NAKEDTRIPLE_H
