//
// Created by drew on 5/16/22.
//

#ifndef SUDOKU_SOLVER_HIDDENSINGLE_H
#define SUDOKU_SOLVER_HIDDENSINGLE_H

#include "Strategy.h"

namespace solver {

class HiddenSingleStrategy : public Strategy {
 public:
  OptionalLogicStep Apply(SudokuGrid& grid) override;
  static OptionalLogicStep CheckForHiddenSingleInSet(SudokuGrid* grid, const std::vector<int>& search_space);
};

} // solver

#endif //SUDOKU_SOLVER_HIDDENSINGLE_H
