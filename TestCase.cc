#include "TestCase.h"

#include <fstream>
#include <iostream>
#include <numeric>
#include <string>

#include "absl/strings/str_split.h"
#include "absl/time/time.h"
#include "Solver.h"

void TestCase::Run() {
  stats_.case_name = name_;
  std::ifstream file(filepath_);
  if (!file.is_open()) {
    std::cout << "Failed to open the file: " << file.rdstate() << std::endl;
    return;
  }
  
  {
    // TODO: Better header handling.
    std::string header;
    std::getline(file, header);
  }
  
  std::ofstream debug_file;
  if (!debug_filename_.empty()) {
    debug_file.open(debug_filename_);
  }
  
  
  std::vector<absl::Duration> durations;
  
  while (file.good()) {
    if (case_limit_ > 0 && stats_.problem_count >= case_limit_) {
      break;
    }
    std::string line;
    std::getline(file, line);
    // Skip empty lines (particularly for the end of the file.
    if (line.empty()) {
      continue;
    }
    // TODO: Maybe clean this up and remove some copies.
    std::pair<std::string, std::string> input_pair = absl::StrSplit(line, ",");
    std::string problem, solution;
    std::tie(problem, solution) = input_pair;
    
    stats_.problem_count++;
    auto grid_or = solver::SudokuGrid::FromString(problem);
    if (!grid_or.ok()) {
      stats_.unparsed_count++;
      continue;
    }
    absl::Time start_time = absl::Now();
    solver::Solver solver(std::move(grid_or).value());
    auto solution_or = solver.solve();
    absl::Duration duration = absl::Now() - start_time;
    if (!solution_or.ok()) {
      switch (solution_or.status().code()) {
        case absl::StatusCode::kUnimplemented:
          stats_.unsolved_count++;
          failed_cases_.push_back(solver.Grid());
          break;
        case absl::StatusCode::kInternal:
          if (debug_file.is_open()) {
            debug_file << "Broken: " << problem << ":" << solution << std::endl;
          }
          stats_.broken_count++;
          break;
        default:
          stats_.unknown_count++;
      }
      continue;
    }
    std::string found_solution = solution_or.value();
    // Check if the solutions match or if the solution is 'empty' by checking to make sure there is a 1.
    if (found_solution != solution && solution.find('1') != std::string::npos) {
      stats_.wrong_count++;
    } else {
      stats_.solved_count++;
      durations.push_back(duration);
    }
  }
  file.close();
  stats_.percent_solved = 100.0 * stats_.solved_count / stats_.problem_count;
  stats_.average_solved_duration =
      std::accumulate(durations.begin(), durations.end(), absl::Duration()) / stats_.solved_count;
  if (stats_.average_solved_duration == absl::InfiniteDuration()) {
    stats_.average_solved_duration = absl::ZeroDuration();
  }
}
