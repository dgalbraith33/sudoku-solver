# Solving Methods

- Good list of techniques: https://old.reddit.com/r/sudoku/wiki/index
- https://www.sudokuwiki.org/sudoku.htm
- Naked Doubles/Triples
- Hidden Doubles/Triples

# Data Banks

## Look Into

- http://sudocue.net
- http://sudokucup.com/

## Ruled Out

- http://websudoku.com: rules against scripting.

## Done

- 2x Kaggle
- http://fiendishsudoku.com
- http://extremesudoku.info

# Puzzle Types

- Killer
- Chess
- Thermo
- Arrow

# Clean up

- Better understand Extreme/Fiendish sudoku code.