#ifndef SUDOKU_SOLVER_SOLVER_H
#define SUDOKU_SOLVER_SOLVER_H

#include <string>

#include "common/LogicStep.h"
#include "grid/SudokuGrid.h"
#include "strategies/Strategy.h"

namespace solver {

class Solver {
 public:
  explicit Solver(const SudokuGrid& grid);
  absl::StatusOr<std::string> solve();
  SudokuGrid Grid() { return grid_; }
  std::string GridDebugString() { return grid_.DebugStringPretty(); }
  void EnableDebugMode() { debug_mode_ = true; }
  
  const std::vector<std::unique_ptr<LogicStep>>& Steps() { return steps_; }
 
 private:
  SudokuGrid grid_;
  bool debug_mode_ = false;
  std::vector<std::unique_ptr<Strategy>> strategies_;
  std::vector<std::unique_ptr<LogicStep>> steps_;
  
  absl::StatusOr<std::unique_ptr<LogicStep>> Iterate();
  absl::Status CheckForBrokenPuzzle();
};

}  // namespace solver


#endif //SUDOKU_SOLVER_SOLVER_H
