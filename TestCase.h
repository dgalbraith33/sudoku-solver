#ifndef SUDOKU_SOLVER_TESTCASE_H
#define SUDOKU_SOLVER_TESTCASE_H

#include <string>
#include <vector>

#include "absl/time/time.h"
#include "grid/SudokuGrid.h"

class TestCase {
 public:
  class Stats {
   public:
    std::string case_name;
    int problem_count = 0;
    int solved_count = 0;
    int unsolved_count = 0;
    int wrong_count = 0;
    int broken_count = 0;
    int unparsed_count = 0;
    int unknown_count = 0;
    double percent_solved = 0.0;
    absl::Duration average_solved_duration;
  };
  
  TestCase(std::string name, std::string filepath) : name_(std::move(name)), filepath_(std::move(filepath)),
                                                     case_limit_(0) {}
  TestCase(std::string name, std::string filepath, int case_limit) : name_(std::move(name)),
                                                                     filepath_(std::move(filepath)),
                                                                     case_limit_(case_limit) {}
  
  void SetDebugPath(const std::string& filename) {
    debug_filename_ = filename;
  }
  
  void Run();
  Stats RunStats() {
    return stats_;
  }
  
  const std::vector<solver::SudokuGrid>& FailedCases() {
    return failed_cases_;
  }
 
 private:
  std::string name_;
  std::string filepath_;
  int case_limit_;
  Stats stats_;
  std::vector<solver::SudokuGrid> failed_cases_;
  
  std::string debug_filename_;
};


#endif //SUDOKU_SOLVER_TESTCASE_H
