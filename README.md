# Sudoku Solver

## About

This solver aims to be a pure-logic solver that solves the puzzle without any guessing or backtracking. As such it is
fast but fails to solve many puzzles for which the necessary logic has yet to be implemented.

## Strategies used

The solver will apply the following strategies in order. Once a strategy is applied it updates the grid then starts at
the beginning of the list again.

1. Naked Singles
2. Hidden Singles
3. Naked Doubles
4. Hidden Doubles
5. Naked Triples
6. Hidden Triples
7. Pointing Pairs
8. Box Line Reduction

## Benchmarks

| Name                           | Count   | Solved  | Unsolved | Percent | Duration |
|--------------------------------|---------|---------|----------|---------|----------|
| 00_kaggle1m                    | 1000000 | 1000000 | 0        | 100%    | 15.9μs   |
| 01_kaggle9m                    | 9000000 | 8845735 | 154265   | 98%     | 16.3μs   |
| 10_sudoku_com_easy             | 1953    | 1953    | 0        | 100%    | 16.7μs   |
| 11_sudoku_com_medium           | 2163    | 2163    | 0        | 100%    | 20.2μs   |
| 12_sudoku_com_hard             | 1075    | 1032    | 43       | 96%     | 166.8μs  |
| 13_sudoku_com_expert           | 986     | 744     | 242      | 75%     | 213.6μs  |
| 14_sudoku_com_evil             | 500     | 0       | 500      | 0%      | 0.0μs    |
| 20_fiendish_sudoku_easy        | 426     | 426     | 0        | 100%    | 26.1μs   |
| 21_fiendish_sudoku_moderate    | 426     | 426     | 0        | 100%    | 31.1μs   |
| 22_fiendish_sudoku_hard        | 426     | 426     | 0        | 100%    | 74.2μs   |
| 23_fiendish_sudoku_fiendish    | 426     | 304     | 122      | 71%     | 188.2μs  |
| 24_fiendish_sudoku_evil        | 426     | 426     | 0        | 100%    | 249.8μs  |
| 25_extreme_sudoku_evil         | 500     | 216     | 284      | 43%     | 215.7μs  |
| 26_extreme_sudoku_excessive    | 500     | 210     | 290      | 42%     | 198.9μs  |
| 27_extreme_sudoku_egregious    | 500     | 187     | 313      | 37%     | 205.0μs  |
| 28_extreme_sudoku_excruciating | 500     | 163     | 337      | 33%     | 238.9μs  |
| 29_extreme_sudoku_extreme      | 500     | 135     | 365      | 27%     | 235.2μs  |


